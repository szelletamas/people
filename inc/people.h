/*
 * people.h
 *
 *  Created on: 2021. febr. 14.
 *      Author: Kicsigamer
 */

#ifndef PEOPLE_H_
#define PEOPLE_H_

enum gender {
	male,
	female
};

/** type of the people */
typedef struct people_t{
	int id; // id of a people
	char* name; //name of the people
	int gender; //gender of the people
	int age; //age of the people
	int weigth; //Weight of the people
}people_t;

/**
 * Creating a people from the received value
 * */
people_t create_people(int id,char* name,int gender,int age,int weigth);

/**
 * free the allocated memory
 * */
void delete_people(people_t* item);

#endif /* PEOPLE_H_ */
