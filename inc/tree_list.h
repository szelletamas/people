/*
 * tree_list.h
 *
 *  Created on: 2021. febr. 26.
 *      Author: Kicsigamer
 */

#ifndef TREE_LIST_H_
#define TREE_LIST_H_

#include <people.h>

#define MAX_CHILD_LIMIT 3

typedef struct tree_element_t{
	int id;
	people_t people;
	struct tree_element_t* child[MAX_CHILD_LIMIT];
}tree_element_t;

typedef struct tree_list_t{
	tree_element_t head;
	int size;
}tree_list_t;

void init_tree_list(tree_list_t* list);

void add_element_to_tree_list(tree_list_t* list,int parent_id,people_t people);

void delete_tree_list(tree_list_t* list);

#endif /* TREE_LIST_H_ */
