/*
 * dynamic_array.h
 *
 *  Created on: 2021. febr. 26.
 *      Author: Kicsigamer
 */

#ifndef DYNAMIC_ARRAY_H_
#define DYNAMIC_ARRAY_H_

/** type of the dynamic array*/
typedef struct dynamic_array_t{
	int size; //size of the array
	int* array; //entry point of the array
}dynamic_array_t;

/*
 * Initializing the list.
 * */
void init_dynamic_array(dynamic_array_t* list);

/*
 * Add plus on item to end of the list
 * */
void dynamic_array_push(dynamic_array_t* list,int value);

/*
 * Deinitializing the list.
 * */
void dynamic_array_delete(dynamic_array_t* list);

#endif /* DYNAMIC_ARRAY_H_ */
