/*
 * tree_list.c
 *
 *  Created on: 2021. febr. 26.
 *      Author: Kicsigamer
 */
#include <stdlib.h>
#include <tree_list.h>

void init_tree_list(tree_list_t* list){
	if(list==NULL){
		return;
	}

	list->head=NULL;
	list->size=0;
}

void add_element_to_tree_list(tree_list_t* list,int parent_id,people_t people){
	if(list==NULL ||
			(parent_id<=-1 && parent_id<list->size) ||
			(list->size>0 && parent_id==-1)){
		return;
	}

	tree_element_t* new_people= (tree_element_t*) malloc(sizeof(tree_element_t));

	new_people->id=list->size;
	new_people->people=people;
	for(int i=0;i<MAX_CHILD_LIMIT;i++){
		new_people->child[i]=NULL;
	}

	if(list->size==0){
		list->head=new_people;
		list->size++;
		return;
	}
}

void delete_tree_list(tree_list_t* list);
