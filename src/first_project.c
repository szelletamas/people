/*
 ============================================================================
 Name        : first_project.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <people.h>
#include <dynamic_array.h>

int main(void) {

	people_t jozsi=create_people(0, "Joska Batyam", male, 44, 78);

	printf("igazolvany szam: %d\tnev: %s\tnem: %d\tkor: %d\tsuly:%d\n",
			jozsi.id,
			jozsi.name,
			jozsi.gender,
			jozsi.age,
			jozsi.weigth);

	delete_people(&jozsi);

	dynamic_array_t list;

	init_dynamic_array(&list);

	dynamic_array_push(&list, 2);
	dynamic_array_push(&list, 7);
	dynamic_array_push(&list, 3);

	dynamic_array_delete(&list);

	return EXIT_SUCCESS;
}
