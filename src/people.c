/*
 * people.c
 *
 *  Created on: 2021. febr. 14.
 *      Author: Kicsigamer
 */
#include <people.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Creating a people from the received value
 * */
people_t create_people(int id,char* name,int gender,int age,int weigth){
	if(!name){
		printf("create people received a null name");
		people_t new_people;
		new_people.age=0;
		new_people.id=0;
		new_people.gender=0;
		new_people.weigth=0;
		new_people.name=NULL;
		return new_people;
	}

	people_t new_people;
	new_people.age=age;
	new_people.id=id;
	new_people.gender=gender;
	new_people.weigth=weigth;

	/**  copy name */
	int lenght=strlen(name);

	char* name_cpy=(char*) malloc((lenght+1)*sizeof(char));

	for(int i=0;i<lenght;i++){
		name_cpy[i]=name[i];
	}

	name_cpy[lenght]='\0';
	new_people.name=name_cpy;
	return new_people;
}


/**
 * free the allocated memory
 * */
void delete_people(people_t* item){
	if(!item){
		return;
	}
	free(item->name);

	item->name=NULL;
}
