/*
 * dynamic_array.c
 *
 *  Created on: 2021. febr. 26.
 *      Author: Kicsigamer
 */

#include <dynamic_array.h>
#include <stdlib.h>


/*
 * Initializing the list.
 * */
void init_dynamic_array(dynamic_array_t* list){
	if(list==NULL){
		return;
	}

	list->size=0;
	list->array=NULL;
}


/*
 * Add plus on item to end of the list
 * */
void dynamic_array_push(dynamic_array_t* list,int value){
	if(list==NULL || (list->size>0 && list->array==NULL)){
		return;
	}

	int* new_array = (int*) malloc((list->size+1)*sizeof(int));

	for(int i=0;i<list->size;i++){
		new_array[i]=list->array[i];
	}

	new_array[list->size]=value;
	free(list->array);
	list->size=list->size+1;
	list->array=new_array;
}

/*
 * Deinitializing the list.
 * */
void dynamic_array_delete(dynamic_array_t* list){
	if(list==NULL){
		return;
	}

	list->size=0;
	free(list->array);
	list->array=NULL;
}
